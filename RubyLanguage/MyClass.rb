class MyClass
  @boo
  def boo
    return @boo
  end
  def boo=(val)
    @boo = val
  end
  def my_method
    @foo = 2
  end
  def self.cls_method
    "MyClass type"
  end
end
